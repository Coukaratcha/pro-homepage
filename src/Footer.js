import React, { Component } from 'react';

class Footer extends Component {
	render() {
		return (
			<footer>
				<div className='container'>
					<p>Mickaël Gillot — <a href='mailto:contact@mickaelgillot.xyz'>contact@mickaelgillot.xyz</a></p>
				</div>
			</footer>
		)
	}
}

export default Footer;